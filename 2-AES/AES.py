# -*- coding: utf-8 -*-
import os
from tkinter import Tk, ttk, filedialog, Radiobutton, IntVar, messagebox
from threading import Thread

#Nk (Longitud de clave) *4
#Nb (Tamaño de bloque)
#Nr (Número de vueltas)

'''
Shows a matrix

@param {Mixed} List to show
'''
def mostrarMatriz(self, lista):
    total = ""
    for i in lista:
        if type(i) == list: mostrarMatriz(i)
        else: total += str('{:02x}'.format(i)) + " "
    print("" if total=="" else "["+total[0:-1]+"]")

class aes():
    # =============================================================================
    # Boxes Defaults
    # =============================================================================
    
    #Round constants box 
    Rcon=bytes([0x1, 0x2, 0x4, 0x8, 0x10, 0x20, 0x40, 0x80, 0x1B, 0x36])
    
    #Substitution Box
    Sbox=bytes([
    0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x1, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
    0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
    0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
    0x4, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x5, 0x9a, 0x7, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
    0x9, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
    0x53, 0xd1, 0x0, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
    0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x2, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
    0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
    0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
    0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
    0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x6, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
    0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x8,
    0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
    0x70, 0x3e, 0xb5, 0x66, 0x48, 0x3, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
    0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
    0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16])
    
    #Inverse Substitution Box
    ISbox=bytes([
    0x52, 0x9, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
    0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
    0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
    0x8, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
    0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
    0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
    0x90, 0xd8, 0xab, 0x0, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x5, 0xb8, 0xb3, 0x45, 0x6,
    0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x2, 0xc1, 0xaf, 0xbd, 0x3, 0x1, 0x13, 0x8a, 0x6b,
    0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
    0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
    0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
    0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
    0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x7, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
    0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
    0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
    0x17, 0x2b, 0x4, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d])

    # =============================================================================
    # GENERIC FUNCTIONS (PERSONALIZED)
    # =============================================================================   
    '''
    Transposes a matrix
    
    @param {Mixed} List to transpose
    
    @return {Mixed} Transposed list
    '''
    transponer = lambda self,lista:list(map(list, zip(*lista)))
    
    '''
    Apply a Xor operation to two list
    
    @param {Mixed} List to xor (a)
    @param {Mixed} List to xor (b)
    
    @return {Mixed} Xor list
    '''
    xor2L = lambda self, a, b: list(map(lambda x,y: x^y, a, b))
    
    '''
    Apply a Multiply to two bytes (using the Russian Peasant Multiplication algorithm)
    
    @param {Byte} Byte to multiply (a)
    @param {Byte} Byte to multiply (b)
    
    @return {Byte} Result Byte
    '''
    def m(self, a, b):
        p = 0
        while (b and a):
            # Si es impar se añade el número anterior
            if (a & 1): p ^= b
            
            # Se multiplica por dos b y se redondea si se sobrepasa
            b <<= 1
            if (b> 0xff): b ^= 0x11b
            
            #Se desplaza a para quitar el bit ya procesado
            a >>= 1
        return p
    
    '''
    Quit padding right
    
    @param {Bytes} List of bytes with padding
    
    @return {Bytes} List clean padding right
    '''
    def padr(self, a):
        a=a[::-1]
        
        fbytes=0
        for i in a:
            if (i != 0): break
            fbytes -= 1
        a=a[::-1]
        if (fbytes==0): return a
        return a[0:fbytes]
        
    # =============================================================================
    # FUNCTIONS TO CIPHER AND INVERSE CIPHER
    # =============================================================================
    '''
    Add a Round Key to the state 
    
    @param {Mixed} State of message
    @param {Mixed} Key / Expansion Key
    
    @return {Mixed} New state of message
    '''
    def AddRoundKey(self, state, key):
        state=self.transponer(state)
        for i in range(0, 4):
            state[i]=self.xor2L(state[i], key[i])  
        return self.transponer(state)
    
    '''
    Rotate array to left
    
    @param {Mixed} Key to rotate
    
    @return {Mixed} Key alredy rotated
    '''
    RotWord = lambda self, key: key[1:] + key[:1]
    
    '''
    Shift the rows of the state.
    
    @param {Mixed} State
    
    @return {Mixed} Shifted state
    '''
    def ShiftRows(self, state):
        for i in range(0,4):
            state[i] = state[i][i:] + state[i][:i]
        return state
    
    '''
    Shift the rows of the state.
    
    @param {Mixed} State
    
    @return {Mixed} Shifted state
    '''
    def InvShiftRows(self, state):
        for i in range(0,4):
            state[i] = state[i][-i:] + state[i][:-i]
        return state
    
    '''
    Replace byte with Sbox
    
    @param {Mixed} Byte
    
    @return {Mixed} Replace Byte
    '''
    SubBytes = lambda self, byteIn: self.Sbox[byteIn]
    
    '''
    Apply SubBytes to Vector
    
    @param {Mixed} Vector
    
    @return {Mixed} Replaced Vector
    '''
    SubWord = lambda self, key: list(map(lambda x: self.SubBytes(x), key))
    
    '''
    Apply SubBytes to Vector
    
    @param {Mixed} Vector
    
    @return {Mixed} Replaced Vector
    '''
    InvSubWord = lambda self, key: list(map(lambda x: self.ISbox[x], key))
    
    '''
    Multiply every column by a matrix
    
    @param {Mixed} State
    
    @return {Mixed} New state
    '''
    def MixColumns(self, state):
        cp = state[:]
        cp = self.transponer(cp)
        state = self.transponer(state)
        for i in range(0, len(state)):
            state[i][0] = self.m(cp[i][0],0x2) ^ self.m(cp[i][1],0x3) ^ cp[i][2] ^ cp[i][3]
            state[i][1] = cp[i][0] ^ self.m(cp[i][1],0x2) ^ self.m(cp[i][2],0x3) ^ cp[i][3]
            state[i][2] = cp[i][0] ^ cp[i][1] ^ self.m(cp[i][2],0x2) ^ self.m(cp[i][3],0x3)
            state[i][3] = self.m(cp[i][0],0x3) ^ cp[i][1] ^ cp[i][2] ^ self.m(cp[i][3],0x2)
        state = self.transponer(state)
        return state
    
    '''
    Multiply every column by an inverse matrix
    
    @param {Mixed} State
    
    @return {Mixed} New state
    '''
    def InvMixColumns(self, state):
        cp = state[:]
        cp = self.transponer(cp)
        state = self.transponer(state)
        for i in range(0, len(state)):
            state[i][0] = self.m(cp[i][0],0xe) ^ self.m(cp[i][1],0xb) ^ self.m(cp[i][2],0xd) ^ self.m(cp[i][3],0x9)
            state[i][1] = self.m(cp[i][0],0x9) ^ self.m(cp[i][1],0xe) ^ self.m(cp[i][2],0xb) ^ self.m(cp[i][3],0xd)
            state[i][2] = self.m(cp[i][0],0xd) ^ self.m(cp[i][1],0x9) ^ self.m(cp[i][2],0xe) ^ self.m(cp[i][3],0xb)
            state[i][3] = self.m(cp[i][0],0xb) ^ self.m(cp[i][1],0xd) ^ self.m(cp[i][2],0x9) ^ self.m(cp[i][3],0xe)
        state = self.transponer(state)
        return state

    # =============================================================================
    # KEY EXPANSION
    # =============================================================================
    '''
    Key Expansion
    
    @param {Mixed} Key to expand
    
    @return {Mixed} Expanded key
    '''
    def KeyExpansion(self, key):
        w=[None] * (self.Nb*(self.Nr+1))
        for i in range(0,self.Nk):
            w[i]=[key[4*i], key[4*i+1], key[4*i+2], key[4*i+3]]
        
        for i in range(self.Nk, self.Nb*(self.Nr+1)): # 4 - 43
            temp = w[i-1]
            if (i%self.Nk == 0):
                temp = self.xor2L(self.SubWord(self.RotWord(temp)), [self.Rcon[(i//self.Nk)-1], 0b0, 0b0, 0b0])
            elif (self.Nk>6 & i%self.Nk == 4):
                temp = self.SubWord(temp)
            
            w[i] = self.xor2L(w[i-self.Nk], temp)
        return w
            

    # =============================================================================
    # CIPHER
    # =============================================================================
    '''
    Cipher message
    
    @param {Mixed} Block message to encrypt (plain)
    @param {Mixed} Key (used to cipher)
    
    @return {String} Cipher block Message (cipher)
    '''
    def Cipher(self, text, key):
        
        # Crear el state
        state = [None] * 4
            
        for i in range(0, 4):
            state[i]=text[(self.Nb*i):(self.Nb*(i+1))]
            
        state = self.transponer(state)
        
        #Se expande la clave
        Kexp = self.KeyExpansion(key)
        
        #Primera Ronda
        state = self.AddRoundKey(state, Kexp[0:self.Nb])
    
        for i in range(1,self.Nr):
            state = list(map(lambda x: self.SubWord(x), state))
            state = self.ShiftRows(state)
            state = self.MixColumns(state)
            state = self.AddRoundKey(state, Kexp[i*self.Nb:((i+1)*self.Nb)])
    
        state = list(map(lambda x: self.SubWord(x), state))
        state = self.ShiftRows(state)
        state = self.AddRoundKey(state, Kexp[self.Nr*self.Nb:(self.Nr+1)*self.Nb])
        
        state = self.transponer(state)
        result = []
        for i in range(0, self.Nb):
            result += state[i]
        
        return result
        
    # =============================================================================
    # INVERSE CIPHER
    # =============================================================================
    '''
    Inverse Cipher message
    
    @param {Mixed} Cipher block Message (cipher)
    @param {Mixed} Key (used to cipher)
    
    @return {String} Block message plain (plain)
    '''
    def InvCipher(self, text, key):
        # Crear el state
        state = [None] * 4
            
        for i in range(0, 4):
            state[i]=text[(self.Nb*i):(self.Nb*(i+1))]
            
        state = self.transponer(state)
        
        #Se expande la clave
        Kexp = self.KeyExpansion(key)
        state = self.AddRoundKey(state, Kexp[self.Nr*self.Nb:(self.Nr+1)*self.Nb])
        
        for i in range(self.Nr-1, 0, -1):
            state = self.InvShiftRows(state)
            state = list(map(lambda x: self.InvSubWord(x), state))
            state = self.AddRoundKey(state, Kexp[i*self.Nb:((i+1)*self.Nb)])
            state = self.InvMixColumns(state)
        
        state = self.InvShiftRows(state)
        state = list(map(lambda x: self.InvSubWord(x), state))
        state = self.AddRoundKey(state, Kexp[0:self.Nb])
        
        state = self.transponer(state)
        result = []
        for i in range(0, self.Nb):
            result += state[i]
        
        return result
    '''
    Generate and write the key in the file introduced. 
    
    @param {String} Name of the file
    @param {Integer} Key length 
    '''
    def generateKey(self, fkey, Nk):
        key = open(fkey, 'w')
        key.write(os.urandom(Nk*4).hex())
        key.close()
    
    '''
    Uses the ECB way of AES to cipher the message. 
    
    @param {String} Name of the file with the message
    @param {String} Name of the destination file to save the encrypted message
    @param {Integer} Key
    
    @return {Boolean} Indicate if the function finished successfully
    '''
    def fCipherECB(self, fplain, fcipher, key):
        global progress
        state = []
        long = len(key)
        self.Nb=4
        
        if (long==16): self.Nk=4; self.Nr=10
        elif (long==24): self.Nk=6; self.Nr=12
        elif (long==32): self.Nk=8; self.Nr=14
        else: return False
        
        fplain = open(fplain, "rb")    
        fcipher = open(fcipher, "wb")
        
        size = len(fplain.read())
        fplain.seek(0)
    
        tblock=size//(self.Nb*4)
        if(size%(self.Nb*4)!=0): tblock += 1
        rblock=tblock
        percent = -1
        
        while True:
            piece = fplain.read(1)
            if (piece == b""): 
                if (len(state)>0):
                    for i in self.Cipher(state+[0] * (self.Nb*4 - len(state)), key):
                        fcipher.write(bytes([i]))
                break
            state.append(int.from_bytes(piece, byteorder='little'))
            if (len(state)==self.Nb*4):
                rblock -=1
                for i in self.Cipher(state, key): fcipher.write(bytes([i]))
                temp=round((1-(rblock/tblock))*100)
                if (percent!=temp):
                    percent = temp
                    progress.set(percent)
                state = []
        
        fcipher.close()
        fplain.close()
        progress.set(0)
        return True
        
    
    '''
    Uses the ECB way of AES to decipher the message. 
    
    @param {String} Name of the destination file to save the message
    @param {String} Name of the file with the encrypted message
    @param {Integer} Key
    
    @return {Boolean} Indicate if the function finished successfully
    '''
    def fICipherECB(self, fplain, fcipher, key):
        global progress
        state = []
        long = len(key)
        self.Nb=4
        
        if (long==16): self.Nk=4; self.Nr=10
        elif (long==24): self.Nk=6; self.Nr=12
        elif (long==32): self.Nk=8; self.Nr=14
        else: return False
        
        fcipher = open(fcipher, "rb")
       
        
        size = len(fcipher.read())
        fcipher.seek(0)
        
        tblock=size/(self.Nb*4)
        if(size%(self.Nb*4)!=0):
            fcipher.close()
            return False
        rblock=tblock
        percent = -1
        
        fplain = open(fplain, 'wb')
    
        while True:
            piece = fcipher.read(1)
            if (piece == b""): break
            state.append(int.from_bytes(piece, byteorder='little'))
            if (len(state)==self.Nb*4):
                rblock -=1
                if (rblock == 0):
                    for i in self.padr(self.InvCipher(state, key)): fplain.write(bytes([i]))
                else:
                    for i in self.InvCipher(state, key): fplain.write(bytes([i]))
                temp=round((1-(rblock/tblock))*100)
                if (percent!=temp):
                    percent = temp
                    progress.set(percent)
                state = []
                
        fcipher.close()
        fplain.close()
        progress.set(0)
        return True
        
    '''
    Uses the CBC way of AES to cipher the message. 
    
    @param {String} Name of the file with the message
    @param {String} Name of the destination file to save the encrypted message
    @param {Integer} Key
    
    @return {Boolean} Indicate if the function finished successfully
    '''
    def fCipherCBC(self, fplain, fcipher, key):
        global progress
        state = []
        long = len(key)
        self.Nb=4
        
        if (long==16): self.Nk=4; self.Nr=10
        elif (long==24): self.Nk=6; self.Nr=12
        elif (long==32): self.Nk=8; self.Nr=14
        else: return False
        
        fplain = open(fplain, "rb")    
        fcipher = open(fcipher, "wb")
        
        size = len(fplain.read())
        fplain.seek(0)
    
        tblock=size//(self.Nb*4)
        if(size%(self.Nb*4)!=0): tblock += 1
        rblock=tblock
        percent = -1
        
        iv=os.urandom(self.Nb*4)
        for i in iv: fcipher.write(bytes([i]))
        
        while True:
            piece = fplain.read(1)
            if (piece == b""): 
                if (len(state)>0):
                    state = self.xor2L(iv, state+[0] * (self.Nb*4 - len(state)))
                    iv = self.Cipher(state, key)
                    for i in iv: fcipher.write(bytes([i]))
                break
            state.append(int.from_bytes(piece, byteorder='little'))
            if (len(state)==self.Nb*4):
                rblock -=1
                state = self.xor2L(iv, state)
                iv = self.Cipher(state, key)
                for i in iv: fcipher.write(bytes([i]))
                temp=round((1-(rblock/tblock))*100)
                if (percent!=temp):
                    percent = temp
                    progress.set(percent)
                state = []
        
        fcipher.close()
        fplain.close()
        progress.set(0)
        return True
    
    '''
    Uses the CBC way of AES to decipher the message. 
    
    @param {String} Name of the destination file to save the message
    @param {String} Name of the file with the encrypted message
    @param {Integer} Key
    
    @return {Boolean} Indicate if the function finished successfully
    '''
    def fICipherCBC(self, fplain, fcipher, key):
        global progress
        state = []
        long = len(key)
        self.Nb=4
        
        if (long==16): self.Nk=4; self.Nr=10
        elif (long==24): self.Nk=6; self.Nr=12
        elif (long==32): self.Nk=8; self.Nr=14
        else: return False
        
        fcipher = open(fcipher, "rb")
       
        
        size = len(fcipher.read())
        fcipher.seek(0)
        
        tblock=size/(self.Nb*4)
        if(size%(self.Nb*4)!=0):
            fcipher.close()
            return False
        rblock=tblock-1
        percent = -1
            
        fplain = open(fplain, 'wb')
        
        ivn = fcipher.read(self.Nb*4)
        
        while True:
            piece = fcipher.read(1)
            if (piece == b""): break
            state.append(int.from_bytes(piece, byteorder='little'))
            if (len(state)==self.Nb*4):
                rblock -=1
                if (rblock == 0):
                    ivo, ivn = ivn, state
                    print(self.InvCipher(ivn, key))
                    for i in self.padr(self.xor2L(ivo,self.InvCipher(ivn, key))): fplain.write(bytes([i]))
                else:
                    ivo, ivn = ivn, state
                    for i in self.xor2L(ivo,self.InvCipher(ivn, key)): fplain.write(bytes([i]))
                temp=round((1-(rblock/tblock))*100)
                if (percent!=temp):
                    percent = temp
                    progress.set(percent)
                state = []
                
        fcipher.close()
        fplain.close()
        progress.set(0)
        return True

# =============================================================================
# INTERFACE AES
# =============================================================================
    
'''
Create a thread which uses the function introduced as a parameter

@param {Mixed} function to uses in the thread
'''
def thread(gen):
    Thread(target=gen).start()        

'''
Change the mode of the buttons

@param {String} new buttons mode
'''
def buttons(mode):
    global bCipher, bDecipher, bKey
    bCipher.config(state=mode)
    bDecipher.config(state=mode)
    bKey.config(state=mode)
    
'''
Take a file name and read the key 2 by 2 bytes

@param {String} file name with the key
'''
def readKey(nkey):  
    key = []
    fkey = open(nkey, 'r')
    while True:
        piece = fkey.read(2)
        if (piece == ""): break
        key.append(int("0x"+piece, 16))
    fkey.close()
    return key

'''
Connect the user interface with the cipher functions
'''
def cifrar():
    win.ptext = filedialog.askopenfilename(title = "Seleccione el fichero a cifrar")
    if (win.ptext == "" or win.ptext == ()):
        messagebox.showerror("Error", "Debe de seleccionar un fichero a cifrar si no no comenzará la operación")
        return
    
    win.ctext = filedialog.asksaveasfilename(title = "Seleccione el fichero donde se va a guardar",filetypes = (("Fichero AES","*.aes"),("Todos los ficheros","*.*")))
    if (win.ctext == "" or win.ctext == ()):
        messagebox.showerror("Error", "Debe de seleccionar un fichero dónde se va a guardar si no no comenzará la operación")
        return
    
    buttons("disabled")
    cod = int(code.get())
    AES=aes()
    if(cod==0): state=AES.fCipherCBC(win.ptext, win.ctext, readKey("Key.txt"))
    else: state=AES.fCipherECB(win.ptext, win.ctext, readKey("Key.txt"))
    
    if(state): messagebox.showinfo("Cifrado","El fichero ha sido cifrado correctamente")
    else: messagebox.showerror("Error", "Umm parece que ha habido un error")
    buttons("normal")
  
'''
Connect the user interface with the decipher functions
'''
def descifrar():
    win.ctext = filedialog.askopenfilename(title = "Seleccione el fichero a descifrar")
    if (win.ctext == "" or win.ctext == ()):
        messagebox.showerror("Error", "Debe de seleccionar un fichero a descifrar si no no comenzará la operación")
        return
    
    win.ptext = filedialog.asksaveasfilename(title = "Seleccione el fichero donde se va a guardar")
    if (win.ptext == "" or win.ptext == ()):
        messagebox.showerror("Error", "Debe de seleccionar un fichero dónde se va a guardar si no no comenzará la operación")
        return
    
    buttons("disabled")
    cod = int(code.get())
    AES=aes()
    if(cod==0): state=AES.fICipherCBC(win.ptext, win.ctext, readKey("Key.txt"))
    else: state=AES.fICipherECB(win.ptext, win.ctext, readKey("Key.txt"))
    
    if(state): messagebox.showinfo("Cifrado","El fichero ha sido descifrado correctamente")
    else: messagebox.showerror("Error", "Umm parece que ha habido un error")
    buttons("normal")

'''
Connect the user interface with the key generate function
'''
def generateKey():
    k=int(bits.get())
    AES=aes()
    if(k==0): AES.generateKey("Key.txt", 4)
    elif (k==1): AES.generateKey("Key.txt", 6)
    else: AES.generateKey("Key.txt", 8)   
    messagebox.showinfo("Nueva Clave","Se ha generado una nueva clave almacenada en Key.txt")

# =============================================================================
# CREATE INTERFACE
# =============================================================================
win = Tk()
win.resizable(False, False)
win.title("AES")

ttk.Label(win,text='Progreso:',justify="left").pack(padx=10, pady=(10,0), ipadx=165)
progress = IntVar()
pbar = ttk.Progressbar(win, variable=progress)
pbar.pack(pady=(10,0),padx=10, ipadx=140)

foption = ttk.Frame(win)
foption.pack(pady=10, padx=5)

fcode = ttk.Frame(foption)
fcode.pack(side="left", padx=(0, 55))
ttk.Label(fcode,text='Método (cifrar/descifrar):',justify="left").pack()
code= IntVar()
Radiobutton(fcode, text='CBC', variable=code, value=0).pack(side="left")
Radiobutton(fcode, text='ECB', variable=code, value=1).pack(side="left")

fbits = ttk.Frame(foption)
fbits.pack(side="left", padx=(0, 30))
ttk.Label(fbits,text='Bits (generar clave):',justify="left").pack(ipadx=55)
bits= IntVar()
Radiobutton(fbits, text='128', variable=bits, value=0).pack(side="left")
Radiobutton(fbits, text='192', variable=bits, value=1).pack(side="left")
Radiobutton(fbits, text='256', variable=bits, value=2).pack(side="left")

acc = ttk.Frame(win)
acc.pack(pady=10, padx=10)
bCipher = ttk.Button(acc,text="Cifrar",command=lambda: thread(cifrar))
bCipher.pack(padx=(20,0),ipadx=1,side="left")
bDecipher = ttk.Button(acc,text="Descifrar",command=lambda: thread(descifrar))
bDecipher.pack(padx=(20,0),ipadx=1,side="left")
bKey = ttk.Button(acc,text="Generar Clave",command=lambda: thread(generateKey))
bKey.pack(padx=(20,0),ipadx=1,side="left")

win.geometry("400x200+{}+{}".format(win.winfo_screenwidth()//2-400//2,
        win.winfo_screenheight()//2-200//2))

win.mainloop()