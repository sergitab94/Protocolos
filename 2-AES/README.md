# AES-128 Cifrado y Descifrado
## Información general
Esta aplicación se encarga de realizar el cifrado en AES. El fichero utilizado que almacena la clave es Key.txt y lo hace en texto plano y en hexadecimal.

<center>![Interfaz AES](https://gitlab.com/sergitab94/Protocolos/raw/master/img/AES.jpg)</center>

El programa cuenta con una interfaz gráfica, en la que encontramos los siguientes elementos:

  * Progreso: Muestra la barra de progreso del cifrado o descifrado de archivos, útil para saber el tiempo que falta al trabajar con archivos grandes.
  * Método (cifrar/descifrar): A través de un radiobutton, se puede elegir cuál de las formas de cifrado/descifrado se quiere elegir, siendo las opciones CBC o ECB.
  * Bits (generar clave): Permite seleccionar la longitud de clave, con la que se elige el tipo de AES que se va a utilizar, AES-128, AES-192 o AES-256.

Por úlitmo encontramos los botones con los que se utilizarán las funciones:

  * Cifrar: Según la clave generada, se utilizará AES-128, AES-192 o AES-256 para cifrar el archivo que se elija a través de la ventana que aparece y que pide seleccionar el archivo que se desea cifrar.
  * Descifrar: Según la clave generada, se utilizará AES-128, AES-192 o AES-256 para descifrar el archivo que se elija a través de la ventana que aparece y que pide seleccionar el archivo que se desea descifrar.
  * Generar clave: Según la longitud de clave seleccionada en bits, genera una clave aletoria que se utilizará para cifrar y descifrar. Esta clave se guardará en el archivo Key.txt.

**Nota:** Los ficheros cifrados están escritos a bajo nivel, es decir, se almacenan los bytes modificados en su representación básica del fichero no en un texto plano. Si desea leer los valores en hexadecimal se tendrá que usar un lector en hexadecimal. Algunos de los lectores recomendados son:

  * GNU/Linux: El sistema suele incluir un lector para leer en hexadecimal el fichero. Si no existe puede instalarse con `apt install xxd`. Para leer un fichero en hexadecimal debe de usarse:
```
xdd [fichero-a-leer]
```
  * Windows: Se puede utilizar [HxD - Freeware Hex Editor](https://mh-nexus.de/en/hxd/). Otra opción es utilizar el PowerShell de Windows cómo lector hexadecimal:
```
cat [fichero-a-leer] | Format-Hex
```

  * MACos: Se puede utilizar el lector [iHex](https://itunes.apple.com/es/app/ihex-hex-editor/id909566003?mt=12)
  
# Explicación de importaciones y primeras variables
## Librerías
### Secrets
Nos permitirá crear valores en hexadecimal aleatorios para las claves.

### Sys
Sys permite realizar escrituas en el terminal de forma avanzada. En este caso sirve para borrar la línea anteriormente escrita y reescribirla con un valor nuevo.
 
## Primeras variables
### Nk
Representa la longitud de la clave

### Nb
Indica el tamñao del bloque

### Nr
Establece el número de iteracciones

### Rcon
Tabla de sustitución utilizada en la expansión de la clave

### Sbox
Tabla de sustitución usada en la trasformación no lineal de bytes

### ISbox
Tabla de sustitución inversa a Sbox

# Explicación de los diferentes métodos
En el siguiente apartado se explicarán las funciones usadas en el cifrado.

## Clase AES
La clase AES es la encargada de hacer los cifrados y descifrados así como la generación de claves.

### mostrarMatriz
```
@param {Mixed} Lista a mostrar
```
Muestra la matriz insertada (o cualquier lista) en un formato fácilmente visible por una person. Además todos los valores son mostrados en hexadecimal, independientemente del tipo insertado.

### transponer
```
@param {Mixed} Lista a transponer

@return {Mixed} Lista transpuesta
```
Transpone en una lista, intercambiando las filas por columnas

### xor2L
```
@param {Mixed} Lista a la que se aplica xor (a)
@param {Mixed} Lista a la que se aplica xor (b)

@return {Mixed} Lista con el xor ya aplicado
```
Aplica al primer valor de la lista "a" el xor sobre el primer valor de la lista "b" y así hasta que se procesa todo la lista, dando cómo resultado una lista a la que se ha aplicado xor.

### m
```
@param {Byte} Byte to multiply (a)
@param {Byte} Byte to multiply (b)

@return {Byte} Result Byte
```
Realiza la multiplicación de un campo finito aritmético. El campo aritmético utilizado es *x<sup>8</sup>+x<sup>4</sup>+x<sup>3</sup>+x+1*. Para realizar este método se ha utilizado el algoritmo `Russina Peasent Multiplication` que se encarga de realizar cualquier operacion de multiplicación en el cuerpo finito.

### padr
```
@param {Bytes} Lista de bytes con padding

@return {Bytes} Lista limpia de padding derecho
```
Elimina el padding derecho de la lista, buscando únicamente valores 0x0.

### AddRoundKey
```
@param {Mixed} Estado del mensaje
@param {Mixed} Clave / Clave expandida

@return {Mixed} Nuevo estado del mensaje
```
Da una nueva vuelta al mensaje

### RotWord
```
@param {Mixed} Clave a rotar

@return {Mixed} Clave ya rotada
```
Pone el primer valor de la clave en el último lugar, girando toda la clave

### ShiftRows
```
@param {Mixed} Estado

@return {Mixed} Estado permutado
```
Permuta hacia la izquierda cada fila su posición empezando por 0.

### InvShiftsRows
```
@param {Mixed} Estado

@return {Mixed} Estado permutado
```
Permuta hacia la derecha cada fila su posición empezando por 0.

### SubBytes
```
@param {Mixed} Byte

@return {Mixed} Remplaza el byte
```
Remplaza el byte en la Sbox

### SubWord
```
@param {Mixed} Vector

@return {Mixed} Vector Remplazado
```
Aplica SybBytes a un vector

### InvSubWord
```
@param {Mixed} Vector

@return {Mixed} Vector Remplazado
```
Aplica la sustitucion con la ISbox

### MixColumns
```
@param {Mixed} State

@return {Mixed} New state
```
Multiplica cada columna por la matriz dada

### InvMixColumns
```
@param {Mixed} State

@return {Mixed} New state
```
Multiplica cada columna por la matriz dada

### KeyExpansion
```
@param {Mixed} Clave a expandir

@return {Mixed} Clave ya expandida
```
Genera una matriz expandida a partir de la clave

### Cipher
```
@param {Mixed} Bloque del mensaje a cifrar (texto plano)
@param {Mixed} Clave (usada para cifrar)

@return {Cadena} Bloque del mensaje cifrado (Cifrado)
```
Cifra un bloque del mensaje

### InvCipher
```
@param {Mixed} Bloque del mensaje cifrado (Cifrado)
@param {Mixed} Clave (usada para descifrar)

@return {Cadena} Bloque del mensaje en texto plano (Plano)
```
Descifra un bloque del mensaje

### generateKey
```
	@param {Cadena} Nombre del fichero
    @param {Integer} Longitud de la clave
```
Genera y escribe la clave en el fichero introducido

### fCipherECB
```
	@param {Cadena} Nombre del fichero con el mensaje
    @param {Cadena} Nombre del fichero destino en el que guardar el mensaje cifrado
    @param {Entero} Clave
    
    @return {Booleano} Indica si la función se terminó correctamente
```
Usa la forma ECB de AES para cifrar el mensaje.

### fICipherECB
```
	@param {Cadena} Nombre del fichero destino donde guardar el mensaje
    @param {Cadena} Nombre del ficher con el mensaje cifrado
    @param {Entero} Clave
    
    @return {Booleano} Indica si la función se terminó correctamente
```
Usa la forma ECB de AES para descifrar el mensaje.

### fCipherCBC
```
	@param {Cadena} Nombre del fichero con el mensaje
    @param {Cadena} Nombre del fichero destino en el que guardar el mensaje cifrado
    @param {Entero} Clave
    
    @return {Booleano} Indica si la función se terminó correctamente
```
Usa la forma CBC de AES para cifrar el mensaje.

### fICipherCBC
```
	@param {Cadena} Nombre del fichero destino donde guardar el mensaje
    @param {Cadena} Nombre del ficher con el mensaje cifrado
    @param {Entero} Clave
    
    @return {Booleano} Indica si la función se terminó correctamente
```
Usa la forma CBC de AES para descifrar el mensaje.

## Funciones de interfaz
A continuación se comentarán las funciones que representan la interfaz de usuario.

### thread
```
	@param {Mixed} Función que se usará en el hilo
```
Crea un hilo que usa la función introducida como parámetro

### buttons
```
	@param {Cadena} Nuevo modo de los botones
```
Cambia el modo de los botones

### readKey
```
	@param {Cadena} Nombre del archivo con la clave
```
Toma el nombre del archovo con la clave y la lee de dos en dos bytes.

### cifrar
Conecta la interfaz de usuario con las fucniones de cifrado.

### descifrar
Conecta la interfaz de usuario con las fucniones de descifrado.

### generateKey
Conecta la interfaz de usuario con las fucniones de generado de claves.