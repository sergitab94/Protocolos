# -*- coding: utf-8 -*-

from hashlib import sha1
from sympy import randprime, isprime
from random import randint

from tkinter import Tk, ttk, Text, messagebox
from threading import Thread

# =============================================================================
# CREATE DSA
# =============================================================================
class dsa():
    
    '''
    Construct the object

    @param {Mixed} Public key (p,q,g,y) 
    @param {Integer} Private key (x)
    ''' 
    def __init__(self, public = -1, private = -1):
        self.public = public
        self.private = private
        if (public == -1 and private == -1): self.genkey()
        
        
    '''
    Modular inverse of 'a' into module 'b'

    @param {Integer} number to calculate the inverse
    @param {Integer} module of the inverse
    
    @return {Integer} modular inverse
    '''
    def mi(self, a, b):
        s=1
        t=0
        orgB=b
        while (b>=2):
            s, t = t, s - (a//b)*t
            a, b = b, a%b
            
        if(b<1): return False
        return t%orgB
    
    '''
    Generate public and private keys for DSA.
    '''
    def genkey(self):        
        q = randprime(2**159+1, 2**160-1)
        while True:
            z = randint((2**1023-1)//(2*q), (2**1024-1)//(2*q))
            p = (2*q*z)+1
            if(isprime(p)): break
        
        
        while True:
            h=randint(2,p-2)
            g=pow(h,((p-1)//q),p)
            if (g>1): break
        
        x=randint(2,q-2)
        y=pow(g, x, p)
        
        self.public=(p,q,g,y)
        self.private=x
        
        
    '''
    Sign the message using the private and public keys

    @param {String} message to sign
    
    @return {Mixed} signed message (r, s)
    '''
    def sign(self,m):
        m = self.sha(m)
        p,q,g,y = self.public

        k = randint(2,q-1)
        r = pow(g, k, p) % q
        temp = self.mi(k,q) * (m + self.private * r)
        s = temp % q

        return (r, s)
        
    '''
    Verify if the message owner of the message is the person who sent it, and
    makes sure the message sent is the message signed.

    @param {String} plain message to check
    @param {Mixed} signature sent
    
    @return {Boolean} check if the plain message is the same as the signed
        message receive
    '''
    def verify(self, m, firm):
        m = self.sha(m)
        p,q,g,y = self.public
        r,s = firm
        if (not(r<q) or not(s<q)): return False
        w = self.mi(s, q)
        u1 = (m*w)%q
        u2 = (r*w)%q
        
        u11 = pow(g,u1,p)
        u22 = pow(y,u2,p)
        v = ((u11 * u22)% p) % q
        return v == r
    
    '''
    Hash in sha1 the message.

    @param {String} plain message to hash
    
    @return {Int} hash of the message in decimal notation
    '''
    sha = lambda self,cad: int(sha1(cad.encode()).hexdigest(),16)

# =============================================================================
# INTERFACE DSA
# =============================================================================
'''
Create a thread which uses the function introduced as a parameter

@param {Mixed} function to uses in the thread
'''
def thread(gen):
    Thread(target=gen).start()

'''
Change the mode of the buttons

@param {String} new buttons mode
'''
def buttons(mode):
    global bGenKey, bSign, bCSign
    bGenKey.config(state=mode)
    bSign.config(state=mode)
    bCSign.config(state=mode)
  
'''
Change the text of the input boxes

@param {Mixed} input boxes to change
@param {String} new input boxes text
'''
def defineE(sub, text):
    sub.delete(0,"end")
    sub.insert(0,text)

'''
Connect the keys generate functions with the user interface
'''
def generar():
    global p, q, g, y, x
    defineE(p,"Generando...")
    defineE(q,"Generando...")
    defineE(g,"Generando...")
    defineE(y,"Generando...")
    defineE(x,"Generando...")
    buttons("disabled")
    DSA = dsa()
    defineE(p,DSA.public[0])
    defineE(q,DSA.public[1])
    defineE(g,DSA.public[2])
    defineE(y,DSA.public[3])
    defineE(x,DSA.private)
    buttons("normal")

'''
Connect the signature functions with the user interface
'''
def firmar():
    global msg, p, q, g, y, x, r, s
    try:
        pub = (int(p.get()), int(q.get()), int(g.get()), int(y.get()))
        priv = int(x.get())
        mess = msg.get("1.0", "end-1c")
    except:
        messagebox.showerror("Error", "Umm parece que ha habido un error")
        return
    
    if(pub[0] == 0 or pub[1] == 0 or pub[2] == 0 or pub[3] == 0 or priv == 0 or mess == ""):
        messagebox.showerror("Error", "Umm parece que ha habido un error")
        return
    
    defineE(r, "Firmando...")
    defineE(s, "Firmando...")
    buttons("disabled")
    DSA = dsa(pub,priv)
    rr, ss = DSA.sign(mess)
    defineE(r, rr)
    defineE(s, ss)
    buttons("normal")

'''
Connect the check functions with the user interface
'''
def verificar():
    global msg, p, q, g, y, r, s
    try:
        pub = (int(p.get()), int(q.get()), int(g.get()), int(y.get()))
        firm = (int(r.get()), int(s.get()))
        mess = msg.get("1.0", "end-1c")
    except:
        messagebox.showerror("Error", "Umm parece que ha habido un error")
        return
    
    if(pub[0] == 0 or pub[1] == 0 or pub[2] == 0 or pub[3] == 0 or firm[0] == 0 or firm[1] == 0 or mess == ""):
        messagebox.showerror("Error", "Umm parece que ha habido un error")
        return
    
    buttons("disabled")
    DSA = dsa(pub,0)
    if(DSA.verify(mess, firm)): messagebox.showinfo("Correcto","Firma correcta")
    else: messagebox.showerror("Incorrecto","Firma no válida") 
    buttons("normal")

# =============================================================================
# CREATE INTERFACE
# =============================================================================
win = Tk()
win.resizable(False, False)
win.title("DSA")

public = ttk.LabelFrame(win, text="Pública", relief="ridge")
public.pack(pady=(10,0), padx=5, ipadx=200)

public1 = ttk.Frame(public)
public1.pack(pady=10)

ttk.Label(public1,text='P:').pack(side="left")
p = ttk.Entry(public1, width=20)
p.pack(side="left")

ttk.Label(public1,text='Q:').pack(side="left", padx=(20,0))
q = ttk.Entry(public1, width=20)
q.pack(side="left")

public2 = ttk.Frame(public)
public2.pack(pady=10)

ttk.Label(public2,text='G:').pack(side="left")
g = ttk.Entry(public2, width=20)
g.pack(side="left")

ttk.Label(public2,text='Y:').pack(side="left", padx=(20,0))
y = ttk.Entry(public2, width=20)
y.pack(side="left")

private = ttk.LabelFrame(win, text="Privada", relief="ridge")
private.pack(pady=(10,0), padx=5, ipadx=200)

ttk.Label(private,text='X:').pack(padx=10, ipadx=180)
x = ttk.Entry(private)
x.pack(padx=10, pady=(5, 10),ipadx=180)

bGenKey = ttk.Button(win,text="Generar claves",command=lambda: thread(generar))
bGenKey.pack(padx=(20,0), pady=(5))

sign = ttk.LabelFrame(win, text="Firma", relief="ridge")
sign.pack(pady=(10,0), padx=5, ipadx=200)

ttk.Label(sign,text='Mensaje:',justify="left").pack(padx=10, ipadx=190)
msg = Text(sign, width = 60, height = 6)
msg.pack(padx=10,pady=(0,20))

sign1 = ttk.Frame(sign)
sign1.pack(pady=10)

ttk.Label(sign1,text='R:').pack(side="left")
r = ttk.Entry(sign1, width=20)
r.pack(side="left")

ttk.Label(sign1,text='S:').pack(side="left", padx=(20,0))
s = ttk.Entry(sign1, width=20)
s.pack(side="left")

acc = ttk.Frame(win)
acc.pack(pady=10, padx=10)
bSign = ttk.Button(acc,text="Firmar",command=lambda: thread(firmar))
bSign.pack(padx=(20,0),ipadx=1,side="left")
bCSign = ttk.Button(acc,text="Comprobar Firma",command=lambda: thread(verificar))
bCSign.pack(padx=(20,0),ipadx=1,side="left")

win.geometry("400x500+{}+{}".format(win.winfo_screenwidth()//2-400//2,
        win.winfo_screenheight()//2-500//2))
win.mainloop()