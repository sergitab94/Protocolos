# Método de firma digital DSA

## Información general
El progrmama firma un mensaje utilizando el algoritmo DSA, además calcula las claves que se podrán usar para la firma.
Las claves también podrán ser introducidas por el usuario para firmar con ellas.

![Interfaz DSA](https://gitlab.com/sergitab94/Protocolos/raw/master/img/DSA.jpg)

El programa cuenta con una interfaz de usuario en donde se muestran, introducen o generan las claves pública y privada.
Los campos de la clave pública se corresponden con:

  * P, Q, G, Y

Los campos de la clave privada se corresponden con:

  * X

Junto a las claves se encuentra un botón:

  * Generar claves: Al pulsar el botón los campos de claves anteriormente explicados se actualizarán con nuevas claves recalculadas.

Posteriormente se encuentra la funcionalidad de firmado, en la que hay los siguientes campos:

    * Mensaje: En este campo se introduce el mensaje que se quiera firmar o comprobar.
    * R y S: Se corresponden con la firma del mensaje que luego se comprobaría.

Al final del documento se encuentran los botones de firmado y comprobación:

    * Firmar: Es el botón que genera R y S firmando el mensaje con las claves pública y privada de la parte de arriba.
    * Comprobar Firma: Es el botón que dado un mensaje, una clave pública y la firma, R y S, comprueba que ese sea el mensaje firmado por esa clave.
    * Ventana de error: Cuando la firma no se corresponde, aparece un mensaje de error.
    * Ventana de información: Cuando la firma del mensaje se comprueba y se corresponden, aparece una ventana de información que lo indica.

#Explicación de los métodos
Los métodos utilizados son los siguientes:

## Clase DSA
La clase DSA es la encargada de realizar todas las funciones que tienen que ver con la firma, como almacenar las claves, calcularlas, firmar o verificar el propietario del mensaje.

### __init__
```python
    @param {Mixed} clave pública (p,q,g,y) 
    @param {Entero} clave privada (x)
```
La función init es la encargada de crear el objeto DSA, estableciendo las claves publica y privada dadas, o llamando a la función "genkey" que las generará en el caso de que no se introduzcan.

### mi
```python
    @param {Entero} numero del que calcular el inverso
    @param {Entero} módulo del inverso
    
    @return {Entero} Inverso modular
```
Inverso modular de 'a' en modulo 'b'

### genkey
Genera las claves pública(p,q,g,y) y privada(x) para la firma DSA.

### sign
```python
    @param {Cadena} mensaje a firmar
    
    @return {Mixed} mensaje firmado (r, s)
```
Firma el mensaje usando el algoritmo de DSA usando las claves privada y pública.

### verify
```python
    @param {Cadena} mensaje en plano que verificar
    @param {Mixed} firma enviada
    
    @return {Booleano} Comprobación de que el mensaje en plano es igual que el mensaje firmado recibido
```
Comprueba si el propietario del mensaje es quien lo envió y lo firmó, y se asegura de que el mensaje recibido es el mensaje firmado.

### sha
```python
    @param {Cadena} mensaje en plano del que calcular el hash
    
    @return {Entero} hash del mensaje en notación decimal
```
Calcula el hash mediante la función SHA1 del mensaje que se le pasa. Este hash se calcula en notación decimal.

## Interfaz
Crea la interfaz gráfica que el usuario verá, y con la que trabajará. A continuación vamos a mostrar las funciones que se usan en ella.

### thread
```
    @param {Mixed} Función que se usará en el hilo
```
Crea un hilo que usa la función introducida como parámetro.

### buttons
```
    @param {Cadena} Nuevo modo de los botones
```
Cambia el modo de los botones.

### defineE
```
    @param {Mixed} campos de entrada a cambiar
    @param {Cadena} nuevos textos que introducir en los campos
```
Cambia el texto de los campos de entrada.

### defineE
Conecta las funciones de generado de claves con la interfaz de usuario.

### firmar
Conecta las funciones de firmado con la interfaz de usuario.

### verificar
Conecta las funciones de verificado con la interfaz de usuario.