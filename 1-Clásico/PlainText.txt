   B�reas y el Sol
B�reas y el Sol disputaban sobre sus poderes, y decidieron conceder la palma al que despojara a un viajero de sus vestidos.

B�reas empez� de primero, soplando con violencia; y apret� el hombre contra s� sus ropas, B�reas asalt� entonces con m�s fuerza; pero el hombre, molesto por el fr�o, se coloc� otro vestido. B�reas, vencido, se lo entreg� al Sol.

Este empez� a iluminar suavemente, y el hombre se despoj� de su segundo vestido; luego lentamente le envi� el Sol sus rayos m�s ardientes, hasta que el hombre, no pudiendo resistir m�s el calor, se quit� sus ropas para ir a ba�arse en el r�o vecino.

   Di�genes de viaje
Yendo de viaje, Di�genes el c�nico lleg� a la orilla de un r�o torrencial y se detuvo perplejo. Un hombre acostumbrado a hacer pasar a la gente el r�o, vi�ndole indeciso, se acerc� a Di�genes, lo subi� sobre sus hombros y lo pas� complaciente a la otra orilla.

Qued� all� Di�genes, reproch�ndose su pobreza que le imped�a pagar a su bienhechor. Y estando pensando en ello advirti� que el hombre, viendo a otro viajero que tampoco pod�a pasar el r�o, fue a buscarlo y lo transport� igualmente. Entonces Di�genes se acerc� al hombre y le dijo:

-No tengo que agradecerte ya tu servicio, pues veo que no lo haces por razonamiento, sino por man�a.


   Androcles y el le�n
Un esclavo llamado Androcles tuvo la oportunidad de escapar un d�a y corri� hacia la foresta.

Y mientras caminaba sin rumbo lleg� a donde yac�a un le�n, que gimiendo le suplic�:

-Por favor te ruego que me ayudes, pues tropec� con un espino y una p�a se me enterr� en la garra y me tiene sangrando y adolorido.

Androcles lo examin� y gentilmente extrajo la espina, lav� y cur� la herida. El le�n lo invit� a su cueva donde compart�a con �l el alimento.

Pero d�as despu�s, Androcles y el le�n fueron encontrados por sus buscadores. Llevado Androcles al emperador fue condenado al redondel del circo a luchar contra los leones.

Una vez en la arena, fue suelto un le�n, y �ste empez� a rugir y buscar el asalto a su v�ctima. Pero a medida que se le acerc� reconoci� a su benefactor y se lanz� sobre �l pero para lamerlo cari�osamente y posarse en su regazo como una fiel mascota.

Sorprendido el emperador por lo sucedido, supo al final la historia y perdon� al esclavo y liber� en la foresta al le�n.