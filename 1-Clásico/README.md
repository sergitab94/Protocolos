# Método de transposición con cifrado y descifrado automatizado
## Información general

El programa cuenta con una interfaz en la terminal que tendrá las siguientes opciones:

  1. Cifrar: Se introduce la clave, en formato numérico, que se corresponderá con k. El archivo traspuesto se guardará en EncryptText.txt.
  2. Descifrar: Para descifrar un archivo, el procedimiento es igual a cifrar, pero el el archivo se guardará en PlainText.txt.
  3. Ataque (Muestra diagrama de fecuencia): se le pedirá al usuario unos máximos y minimos de k para atacar por fuerza bruta el cifrado. Tras esto, una función interna filtra cuál de los posibles resultados es el correcto, y lo almacena en AttackText.txt. Además, para que el usuario pueda ver de forma gráfica que el cifrado es un método de transposición y que se mantienen las frecuencias de las letras del lenguaje, se le muestra una gráfica de frecuencias.
  0. Salir: Sale del programa.

Los diccionarios, tanto de español cómo de inglés, deben de estar organizados por la longitud de las palabras. Los diccionarios diponibles están en los ficheros:

  * PalabrasESordenado.txt: Diccionario en español
  
  * WordENsort.txt: Diccionario en inglés
 
El comado UNIX utilizado ha sido:

```shell
#!/bin/bash
awk '{print length, $0}' diccionario.txt | sort -nr | cut -d " " -f2- > ordenado.txt
```

# Explicación de los diferentes métodos utilizados
En este apartado se explican los diferentes procedimientos realizados en el sistema

## coTransposicion(mensaje, k)
```python
@param {Cadena} Mensaje a encriptar (Texto plano)
@param {Entero} Número de de columnas (Clave)

@return {Cadena} Mensaje transpuesto (Cifrado)
```

El mensaje original se transpone en función de la clave introducida y se devuelve el mensaje transpuesto.

## decoTransposicion(mensaje, k)
```python
@param {Cadena} Mensaje a descifrar (Texto cifrado)
@param {Entero} Número de de columnas (Clave)

@return {Cadena} Mensaje plano (Texto plano)
```

El mensaje cifrado se transpone en función de la clave introducida y se devuelve el mensaje original.

## puntuacion(mensaje, fich)
```python
@param {Cadena} Mensaje a evaluar
@param {Cadena} Diccionario de palabras

@return {Entero} Puntuación del mensaje
```

Dada una cadena a evaluar y un diccionario, se puntua a la cadena con el número de palabras del diccionario que se encuentren. Este procedimiento devuelve un entero con la puntuación correspondiente. Este será el número mínimo de palabras disponibles (por eso el diccionario está ordenado con las palabras más largas al inicio y las palabras más cortas al final).

## seleccion(mensajes, fich)
```python
@param{Lista[Cadena]} Lista en la que buscar el mensaje
@param{Cadena} Diccionario de palabras

@return {Lista[Cadena]} Clave y mensaje localizado
```

A la función se le pasa una lista de posibles mensajes desencriptados. A esta lista se le aplica la función de "puntuacion" y se queda con la clave y la cadena que obtenga una puntuación menor, que será la que tenga mas sentido, puesto que tiene la coincidencia de palabras mas largas.

## arreglarTexto(cadena)
```python
@param {Cadena} Mensaje a limpiar
@return {Cadena} Mensaje limpio
```

A la cadena introducida se le pasa un filtro de normalización:
 - Pasamos el texto a minúsculas.
 - Sustituimos los carácteres con tilde o diéresis por el caracter sin ella.
 - Quitamos los carácteres que no sean letras.


## graficaFrecuencias(mensaje, abecedario)
```python
@param {Cadena} Mensaje sobre el que calcular la frecuencia.
@param {Cadena} Abecedario
```

Función que dibuja una gráfica con la frecuencia de los carácteres según el mensaje introducido.

## menu\*()

Son las diferentes funciones que se encargan de mostrar el menú y de llamar a las funciones correspondientes para hacer la tarea solicitada.

## error()

Imprime un mensaje de error.
