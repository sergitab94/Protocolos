# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt


# =============================================================================
# ENCRYPT FUNCTIONS
# =============================================================================
'''
Transpose encrypt

@param {String} Message to encrypt (plain)
@param {Integer} Number of columns (key)

@return {String} Message transpose (cipher)
'''
def coTransposicion(mensaje, k):
    k = int(k)
    mensaje = arreglarTexto(mensaje)
    #filas
    s = int(len(mensaje)/k)
    if(len(mensaje)%k != 0): s+=1
    mensaje += ' ' *int(k-len(mensaje)%k)
    a = []
    
    for fil in range(s):
        b = []
        for col in range(k):
            b.append(mensaje[col+(fil*k)])
        a.append(b)
            
    sol = ""
    for col in range(k):
        for fil in range(s):
            sol += a[fil][col]
    return sol

# =============================================================================
# DECRYPT FUNCTIONS
# =============================================================================
'''
Transpose decrypt

@param {String} Message to decrypt (cipher)
@param {Integer} Number of columns (key)

@return {String} Message plain (plain)
'''
def decoTransposicion(mensaje, k):
    k = int(k)
    mensaje = arreglarTexto(mensaje)
    #filas
    s = int(len(mensaje)/k)
    if(len(mensaje)%k != 0): s+=1
    mensaje += ' ' *int(k-len(mensaje)%k)
    a = []
    for col in range(k):
        b = []
        for fil in range(s):
                b.append(mensaje[fil+(col*s)])
        a.append(b)
            
    sol = ""
    for fil in range(s):
        for col in range(k):
            sol += a[col][fil]
    return sol

'''
It gives a score to the message

@param {String} Message to evaluate
@param {String} Dictionary of words

@return {Integer} Score for the message
'''
def puntuacion(mensaje, fich):
    dic = open(fich, 'r')
    count = 0
    for line in open(fich): count += 1
    punt = 0
    for i in range(count):
        palabra = dic.readline()
        palabra = palabra.strip('\n')
        suma = mensaje.count(palabra)
        if(mensaje.find(palabra) > -1):
            mensaje = mensaje.replace(palabra, '')
            punt += suma
    return punt

'''
It choose the message with the lower score

@param{List[String]} List to search the message
@param{String} Dictionary of words

@return {List[String]} Key and message decrypt
'''
def seleccion(mensajes, fich):
    if not mensajes:
        return ""
    punt = puntuacion(mensajes[0], fich)
    sol = [0, mensajes[0]]
    for i in range(1, len(mensajes)):
        punt1 = puntuacion(mensajes[i], fich)
        if (punt1 < punt):
            punt = punt1
            sol = [i, mensajes[i]]
    return sol

# =============================================================================
# GENERIC FUNCTIONS
# =============================================================================
'''
It removes strange characters

@param {String} Message to clean
@return {String} Message cleaned 
'''
def arreglarTexto(cadena):
    s = cadena.lower()

    s = s.replace("á","a")
    s = s.replace("é","e")
    s = s.replace("í","i")
    s = s.replace("ó","o")
    s = s.replace("ú","u")
    s = s.replace("ü","u")
    
    # bucle para eliminar caracteres que no sean letras ni espacios
    S = ''
    for i in s:
        k = ord(i)
        if (k>96 and k<123) or (k == 241) or (k == 164) or (k == 32):
            S = S + i
    return S

'''
It shows frequency chart

@param {String} Message which calculates frequency 
@param {String} Alphabet
'''
def graficaFrecuencias(mensaje, abecedario):
    
    mensajelocal = arreglarTexto(mensaje)
    p = [] 
    long = len(mensajelocal)
        
    for i in range(0,len(abecedario),1):
        p.append(mensajelocal.count(abecedario[i])/long)
    
    #para el espacio
    p.append(mensajelocal.count(chr(32))/long)
    
    # Creamos el gráfico
    plt.plot(p)
    plt.plot([])
    plt.title("Probabilidades")
    plt.xlabel("carácter") 
    plt.ylabel("probabilidad") 
    indice = np.arange(len(abecedario)+1)
    plt.xticks(indice, abecedario+"_")
    plt.show()
    print("\n")

# =============================================================================
# MENU FUNCTIONS
# =============================================================================
def menuencrypt():
    while True:
        num=input("Introduzca la clave de cifrado (numérico): ")
        k=0
        try:
            k=int(num)
            if (k<=0):
                error()
                continue
        except:
            error()
            continue
        pt = open("PlainText.txt", 'r')
        text = coTransposicion(pt.read(), k)
        et = open("EncryptText.txt", 'w')
        et.write(text)
        pt.close()
        et.close()
        print("Se ha guardado el mensaje cifrado correctamente en '" + '\x1b[4;32;49m' + "EncryptText.txt" + '\x1b[0m' + "'\n\n")
        return

def menudecrypt():
    while True:
        num=input("Introduzca la clave de cifrado (numérico): ")
        k=0
        try:
            k=int(num)
            if (k<=0):
                error()
                continue
        except:
            error()
            continue
        et = open("EncryptText.txt", 'r')
        text = decoTransposicion(et.read(), k).strip()
        pt = open("PlainText.txt", 'w')
        pt.write(text)
        et.close()
        pt.close()
        print("Se ha guardado el mensaje descifrado correctamente en '" + '\x1b[4;32;49m' + "PlainText.txt" + '\x1b[0m' + "'\n\n")
        return
    
def menuattack():
    while True:
        num1=input("Introduzca la clave de cifrado inferior (numérico): ")
        i=0
        try:
            i=int(num1)
            if (i<=0):
                error()
                continue
        except:
            error()
            continue
        
        num2=input("Introduzca la clave de cifrado superior (numérico): ")
        k=0
        try:
            k=int(num2)
            if (k<i):
                error()
                continue
        except:
            error()
            continue

        lista1 = list()
        et = open("EncryptText.txt", 'r')
        msg = et.read()
        for z in range(i,k+1):
            lista1.append(decoTransposicion(msg, z))
        es = seleccion(lista1, 'PalabrasESordenado.txt')
        en = seleccion(lista1, 'WordsENsort.txt')
        
        text = "Español (k=" + str(es[0]+i) + "): " + es[1].strip() + "\n\n"
        text += "English (k=" + str(en[0]+i) + "): " + en[1].strip()
        at = open("AttackText.txt", 'w')
        at.write(text)
        graficaFrecuencias(msg, "abcdefghijklmnopqrstuvwxyzñ")
        et.close()
        at.close()
        print("Se ha guardado el ataque correctamente en '" + '\x1b[4;32;49m' + "AttackText.txt" + '\x1b[0m' + "'\n\n")
        return
    
def error():
    print('Valor introducido erróneo')

main = """---Menú Principal---
La información en texto plano para cifrar está en 'PlainText.txt'.
El texto cifrado se almacena en 'EncryptText.txt'.
Los resultados de un ataque serán guardados en 'AttackText.txt'.
1. Cifrar
2. Descifrar
3. Ataque (Muestra diagrama de frecuencia)
0. Salir"""

while True:
    print(main)
    switcher = {1: menuencrypt, 2: menudecrypt, 3: menuattack }
    arg=input("Seleccione una opción: ")
    if arg=="0": break
    try:
        funciona = switcher.get(int(arg), error)
        funciona()
    except:
        error() 