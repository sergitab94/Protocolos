# Protocolos Seguros
Este proyecto contiene las prácticas realizadas en la asignatura de Protocolos Seguros, cursada el primer semestre del curso 2018-2019.

Cada ejercicio está compuesto por el código junto a un README.md que contendrá toda la información acerca de cómo se ha escrito toda la información. Para convertir todos el texto en formato markdown a PDF se ha utilizado [Markdown to PDF](https://www.markdowntopdf.com/).

El repositorio con el código de esta aplicación se encuentra en la siguiente dirección: [https://gitlab.com/sergitab94/Protocolos](https://gitlab.com/sergitab94/Protocolos)

Autores:

  * Jimeno de la Calle, Diego
  * Tabarés Medina, Sergio