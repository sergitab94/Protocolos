# Hashing SHA-1

# Método de hashing utilizando SHA-1
## Información general

![Interfaz SHA-1](https://gitlab.com/sergitab94/Protocolos/raw/master/img/SHA-1.jpg)

El metodo calcula el hash en formato SHA-1 del fichero seleccionado tras dar al botón Hashear.

El hash calculado se mostrará en la interfaz y además se almacenará en el fichero:
  
  * HashText.txt

# Explicación de las constantes utilizadas
En este apartado se explican las distintas constantes utilizadas a lo largo del programa

## iHash
Son los 5 valores iniciales con los que se calcula el hash SHA-1

## K
Valores de K que se usan a lo largo del hash de SHA-1

## msk
Máscara utilizada en hexadecimal para calcular los módulos en el metodo alternativo de calcular SHA-1

## MASK 
Máscara utilizada para para truncar los resultados en bits y que no se produzca overflow, desbordamiento.

# Explicación de los métodos utilizados
A continuación explicamos el funcionamiento de las funciones creadas y utilizadas para la producción del hash.

## pad
```python
@param {Entero} Bloque a añadir padding
@param {Entero} Longitud del bloque
@param {Entero} Tamaño del archivo 

@return {Entero} Bloque con el padding añadido
```
Añade un padding al último bloque del que calcular el hash.

## addbit
```python
@param {Entero} Bloque al que añadir un bit

@return {Entero} Bloque con el bit añadido
```
Añade un bit a 1 en la cadena que se le introduce.

## toList
```python
@param {Entero} Bloque a convertir

@return {Mixed} Lista de fragmentos del bloque
```
Convierte el bloque a una lista de un tamaño de 32 bits cada uno.

## rotL
```python
@param {Entero} Bloque a rotar
@param {Entero} Numero de permutaciones

@return {Entero} Bloque permutado
```
Permuta el bloque de bits hacia la derecha un numero de permutaciones introducidas.

## sha1
```python
@param {Mixed} Fichero abierto

@return {Cadena} Hash del fichero
```
Calcula el hash en SHA-1 del fichero introducido, y devuelve una cadena que será el hash calculado.

Para calcular el hash, se utiliza el método alternativo de implementación que sobreescribe los valores de la lista en la que se calcula la solución con lo que se ahorra memoria.