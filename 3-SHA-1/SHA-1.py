# -*- coding: utf-8 -*-
# =============================================================================
# DEFAULT VALUES
# =============================================================================

from tkinter import Tk, ttk, filedialog
from threading import Thread

class sha():
    # Inital values of hash 
    iHash = [0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0]
    
    # Constant of use in hashing
    K = [0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xca62c1d6]
    
    # Mask used as module
    msk = 0xf
    
    # Mask used to truncate
    MASK = 2**32-1
        
    # =============================================================================
    # GENERIC FUNCTIONS
    # =============================================================================
    '''
    Pad the final block
    
    @param {Integer} Block to pad
    @param {Integer} Block length
    @param {Integer} File size 
    
    @return {Integer} Padded block
    '''
    def pad(self, block, long, size):
        t = block << (447 - (long*8))
        return t<<64 ^ (size*8)
    
    '''
    Add final bit
    
    @param {Integer} Block to add bit
    
    @return {Integer} Block with bit added
    '''
    def addbit(self, block):
        return (block<<1) ^ 0b1    
    
    '''
    Convert number to list
    
    @param {Integer} Block to convert
    
    @return {Mixed} List of block slices
    '''
    def toList(self, x, bits):
        list = []
        for i in range(bits//32, 0, -1):
            list.append((x>>(i-1)*32) & self.MASK)
        return list
    
    '''
    Rotate to left
    
    @param {Integer} Block to rotate
    @param {Integer} Number of rounds
    
    @return {Integer} Rotated block
    '''
    def rotL(self, x, n):
        return 2**32-1 & ((x<<n) | (x >> (32 - n)))
    
    # =============================================================================
    # HASHING
    # =============================================================================
    '''
    Hashing in SHA-1
    
    @param {Mixed} Open file
    
    @return {String} Hash of file
    '''
    def sha1(self, ofile):
        ofile=open(ofile, 'rb')
        Hash=self.iHash[:]
        BHash=self.iHash[:]
        size = len(ofile.read())
        ofile.seek(0)
    
        padd=False
        rounds = (size // 64) + 1
        if (size % 64 >=56):
            rounds += 1 
            padd=True
        
        for j in range(rounds, 0, -1):
            block=ofile.read(64)
            bsize = len(block)
            
            if (block == b""): block = 0
            else: block = int.from_bytes(block , byteorder='big')
            
            if(j==1 and not(padd)): #Vaso sin ser llenado y sin necesidad de segundo
                tblock=self.addbit(block)
                tblock = self.pad(tblock, bsize , size)
            elif(j==1 and padd): # Vaso sin ser llenado y con necesidad de segundo (ronda final)
                tblock = self.pad(block, bsize , size)
            elif(j==2 and padd): #Vaso sin ser llenado y con necesidad de segundo (ronda penúltima)
                tblock=self.addbit(block)
                tblock=tblock << 511-(bsize*8)
            else: #Vaso llenado y con necesidad de segundo (vacío o lleno)
                tblock = block << 512-(bsize*8)
            
            tblock = self.toList(tblock, 512)
            a=Hash[0]
            b=Hash[1]
            c=Hash[2]
            d=Hash[3]
            e=Hash[4]
            for t in range(0,80):
                s = (t & self.msk)
                if (t>=16):
                    temp = tblock[(s+13)&self.msk] ^ tblock[(s+8)&self.msk] ^ tblock[(s+2)&self.msk] ^ tblock[s]
                    tblock[s] = (self.rotL(temp ,1)) & self.MASK
                    
                if (t <= 19):
                    k = self.K[0]
                    f = (b & c) ^ ((self.MASK^b) & d)
                elif (t <= 39):
                    k = self.K[1]
                    f = b ^ c ^ d
                elif (t <= 59):
                    k = self.K[2]
                    f = (b & c) ^ (b & d) ^ (c & d)
                else:
                    k = self.K[3]
                    f = b ^ c ^ d
                
                y = tblock[s]
                T = (self.rotL(a, 5) + f + e + k + y) & self.MASK
                e = d
                d = c
                c = (self.rotL(b, 30)) & self.MASK
                b = a
                a = T
                
                BHash[0] = (a + BHash[0]) & self.MASK
                BHash[1] = (b + BHash[1]) & self.MASK
                BHash[2] = (c + BHash[2]) & self.MASK
                BHash[3] = (d + BHash[3]) & self.MASK 
                BHash[4] = (e + BHash[4]) & self.MASK
            
            Hash[0] = (Hash[0] + a) & self.MASK
            Hash[1] = (Hash[1] + b) & self.MASK
            Hash[2] = (Hash[2] + c) & self.MASK
            Hash[3] = (Hash[3] + d) & self.MASK 
            Hash[4] = (Hash[4] + e) & self.MASK
            
        s=''
        for h in Hash:
            s+=str('{:08x}'.format(h))   
        ofile.close()
        return s

# =============================================================================
# INTERFACE SHA1
# =============================================================================
def defineE(sub, text):
    sub.delete(0,"end")
    sub.insert(0,text)

def mSHA():
    win.filename = filedialog.askopenfilename(title = "Seleccione fichero a hashear")
    if (win.filename == "" or win.filename == ()):
        defineE(tsha, "Umm ha habido un error")
        return
    defineE(tsha, "Procesando...")
    text = sha().sha1(win.filename)
    fhash=open("HashText.txt", 'w')
    fhash.write(text)
    fhash.close()
    defineE(tsha, text)
    
    
def thread(gen):
    Thread(target=gen).start()

# =============================================================================
# CREATE INTERFACE
# =============================================================================
win = Tk()
win.resizable(False, False)
win.title("SHA")

ttk.Label(win,text='Hash resultante (guardado en HashText.txt):',justify="left").pack(padx=10, pady=(10,0), ipadx=190)
tsha = ttk.Entry(win)
tsha.pack(padx=10, pady=5, ipadx=150)

ttk.Button(win,text="Hashear",command=lambda: thread(mSHA)).pack()

win.geometry("400x90+{}+{}".format(win.winfo_screenwidth()//2-400//2,
        win.winfo_screenheight()//2-90//2))

win.mainloop()