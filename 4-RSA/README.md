# Método de cifrado asimetrico RSA

## Información general
El metodo cifra y descifra mensajes con el algoritmo RSA, además de calcular las claves seguras con las que realizar las operaciones.

![Interfaz RSA](https://gitlab.com/sergitab94/Protocolos/raw/master/img/RSA.jpg)

El programa cuenta con una interfaz gráfica en la que se generan las claves con el botón "Generar Claves", y coloca las claves de la siguiente forma:

  * Módulo: Escribe el módulo en el que se cifra y se descifran los mensajes.
  * Pública: Escribe el exponente usado como clave pública para cifrar.
  * Privada: Escribe el exponente usado como clave privada para descifrar.

Los mensajes se corresponden con lo siguiente:

  * Mensaje: Aquí se escribe el mensaje en plano que se quiera cifrar, o aparecerá el mensaje descifrado al descifrar.
  * Cifrado: Aquí se escribe el mensaje cifrado que se quiera descifrar, o aparecerá el mensaje cifrado al cifrar.

Con las consideraciones anteriores, tenemos otros dos botones:

  * Cifrar: Se encarga de realizar las funciones de cifrado, toma el texto plano y usando el módulo y la cláve pública, genera el mensaje cifrado que aparecerá en el campo de "Cifrado".
  * Descifrar: Se encarga de realizar las funciones de descifrado, toma el texto cifrado, y usando el módulo y la cláve privada, genera el mensaje en plano que aparecerá en el campo "Mensaje".

# Explicación de las constantes utilizadas
## dicc
Es la variable de tipo String en la que se guardan los carácteres del diccionario. Al estár en el orden, permite traducir un mensaje de texto a numero o viceversa.

# Explicación de los métodos utilizados
A continuación explicaremos las funciones utilizadas para cifrar y descifrar los mensajes:

## Clase RSA
La clase RSA, contiene los métodos usados para la generación de primos, así como calcular cifrados y descifrados.

### __init__
```python
    @param {Mixed} clave pública (exponente, modulo) 
    @param {Entero} clave privada (exponente, modulo)
```
Construye el objeto

### randkey
```python
    @param {Entero} Longitud de la clave aleatoria
```
Genera claves aleatorias con un numeros primos aleatorios.

### randkey
```python
    @param {Entero} Longitud de la clave aleatoria
```
Genera claves aleatorias con un numeros primos aleatorios.

### generatekeys
Utiliza p y q para generar las claves pública y privada.

### cipher
```python
    @param {Cadena} mensaje a cifrar
    
    @return {Entero} mensaje cifrado
```
Utiliza el algoritmo RSA para cifrar mensajes utilizando la clave pública.

### icipher
```python
    @param {Entero} mensaje a descifrar
    
    @return {Cadena} mensaje cesicrado
```
Utiliza el algoritmo RSA para descifrar mensajes utilizando la clave privada.

### toNumber
```python
    @param {Cadena} mensaje a convertir en número
    
    @return {Mixed} mensaje convertido en número
```
Usando un diccionario, la función traduce un string en una lista. La longitud de cada miembro de la lista depende de la longitud de la clave.

### toString
```python
    @param {Entero} mensaje numerico a convertir en string
    
    @return {Mixed} mensaje convertido a string
```
Usando el diccionario, la función traduce de numero a string.

### mi
```python
    @param {Entero} numero del que calcular el inverso
    @param {Entero} módulo del inverso
    
    @return {Entero} Inverso modular
```
Inverso modular de 'a' en modulo 'b'

### mi
```python
    @param {Entero} numero del que calcular el inverso
    @param {Entero} módulo del inverso
    
    @return {Entero} Inverso modular
```
Inverso modular de 'a' en modulo 'b'

## Funciones de interfaz
En el siguiente apartado veremos las funciones utilizadas para la interfaz.

### thread
```python
    @param {Mixed} función que utilizará el hilo
```
Crea un hilo que ejecutará la función introducida como parámetro.

### buttons
```python
    @param {Cadena} nuevo modo del botón
```
Cambia el modo de los botones.

### defineE
```python
    @param {Mixed} campos de entrada que actualizar
	@param {String} campos de entrada actualizados
```
Cambia el texto que se muestra en las entradas de texto.

### defineT
```python
    @param {Mixed} campos de texto que actualizar
	@param {String} campos de texto actualizados
```
Cambia el texto que se muestra en los campos de texto.

### generar
Genera p, q y las claves pública y privada.

### cifrar
Conecta las funciones de cifrado con la interfaz gráfica.

### descifrar
Conecta las funciones de descifrado con la interfaz gráfica.

### arreglarTexto
```python
    @param {Cadena} mensaje a limpiar

	@return {String} mensaje limpio
```
Borra los carácteres extraños. Solo permite los carácteres que se encuentran en el diccionario.

### Interfaz
Crea la interfaz gráfica que el usuario verá, y con la que trabajará.