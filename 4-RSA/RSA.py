# -*- coding: utf-8 -*-

from sympy import randprime, log, isprime
from tkinter import Tk, ttk, Text
from threading import Thread

# =============================================================================
# CREATE RSA
# =============================================================================
class RSA():
    dicc = " abcdefghijklmnopqrstuvwxyz"
    
    '''
    Construct the object

    @param {Mixed} Public key (exponent, module) 
    @param {Integer} Private key (exponent, module)
    '''
    def __init__(self, public = -1, private = -1):
        self.public = public
        self.private = private
        if (public == -1 and private == -1): self.randkey(1024)
      
    '''
    Generate random keys with random prime numbers

    @param {Integer} length of the random key
    '''
    def randkey(self, long):
        
        while True:
            r = randprime(2**(long-1), 2**(long))
            if (isprime(r*2+1)): break
        
        
        while True:
            s = randprime(2**(long-1), 2**(long))
            if (r!=s and isprime(s*2+1)): break
        
        self.p=2*r+1
        self.q=2*s+1
        self.generatekeys()
        
    '''
    Use p and q for genertate public and private keys
    '''
    def generatekeys(self):
        p=self.p
        q=self.q
        n = p * q
        phi = (p-1) * (q-1)        
        e = 65537
        d = self.mi(e, phi)
        
        self.public = (e, n)
        self.private = (d, n)
        
    '''
    Use RSA algorithm to cipher a message using public key

    @param {String} message to cipher
    
    @return {Integer} encripted message
    '''
    def cipher(self, msg):
        l = [pow(j, self.public[0], self.public[1]) for j in self.toNumber(msg)]
        n = '%0' + str(len(str(self.public[1]))) + 'd'
        return int(''.join(map(lambda x: n % x, l)))
    
    '''
    Use RSA algorithm to decipher a message using private key

    @param {Integer} message to decipher
    
    @return {Cadena} decipher message
    '''
    def icipher(self, msg):
        lm = len(str(msg))
        lc = len(str(self.public[1]))        
        lb = (lm//lc)*lc        
        if (lm%lc>0): lb += lc
        n = '%0' + str(lb) + 'd'
        msg = n % msg
        l = [int(msg[i:i+lc]) for i in range(0,lm,lc)]              
        return (''.join([self.toString(pow(char,self.private[0],self.private[1])) for char in l])).lstrip()
    
    '''
    Using a dictionary, the function translate a string into list of numbers.
    The lengt of each member of the list depends on the length of the key.

    @param {String} message to convert into number
    
    @return {Mixed} message converted into numbers
    '''
    def toNumber(self, msg):
        n = self.public[1]
        d = len(self.dicc)
        nlb = int(log(n, d))
        tot = []

        rest = len(msg)%nlb
        if (rest != 0):
            bloq = 0
            for j in range(0, rest):
                bloq += self.dicc.find(msg[j])*(d**(rest-j-1))
                
            tot.append(bloq)
            
        msg = msg[rest::]
        
        for i in range(0, len(msg)//nlb):       #bloques
            bloq = 0
            for j in range(0, nlb):             #letras
                bloq += self.dicc.find(msg[j+(i*nlb)])*(d**(nlb-j-1))
            tot.append(bloq)
            
        return tot
    
    '''
    Using a dictionary, the function translate a number into string.

    @param {Integer} number message to convert into string
    
    @return {Mixed} message converted into string
    '''
    def toString(self, msg):
        d = len(self.dicc)
        bloq = ""
        nlb = int(log(self.private[1], len(self.dicc)))
        
        while (nlb>0):
            bloq += self.dicc[msg%d]
            msg = msg // d
            nlb -=1
    
        return bloq[::-1]
    
    '''
    Modular inverse of 'a' into module 'b'

    @param {Integer} number to calculate the inverse
    @param {Integer} module of the inverse
    
    @return {Integer} modular inverse
    '''
    def mi(self, a, b):
        s=1
        t=0
        orgB=b
        while (b>=2):
            s, t = t, s - (a//b)*t
            a, b = b, a%b
            
        if(b<1): return False
        return t%orgB

# =============================================================================
# INTERFACE RSA
# =============================================================================
'''
Create a thread which uses the function introduced as a parameter

@param {Mixed} function to uses in the thread
'''
def thread(gen):
    Thread(target=gen).start()
    
'''
Change the mode of the buttons

@param {String} new buttons mode
'''
def buttons(mode):
    global bgen, bCipher, bDecipher
    bgen.config(state=mode)
    bCipher.config(state=mode)
    bDecipher.config(state=mode)
  
'''
Change the text of the input boxes

@param {Mixed} input boxes to change
@param {String} new input boxes text
'''
def defineE(sub, text):
    sub.delete(0,"end")
    sub.insert(0,text)
   
'''
Change the text of the text boxes

@param {Mixed} text boxes to change
@param {String} new text boxes text
'''
def defineT(sub, text):
    sub.delete(0.0,"end")
    sub.insert(0.0,text)
    
'''
Generate p, q and the public and private keys
'''
def generar():
    global p, q, e, pub, priv
    defineE(p,"Generando...")
    defineE(q,"Generando...")
    defineE(modu,"Generando...")
    defineE(pub,"Generando...")
    defineE(priv,"Generando...")
    buttons("disabled")
    rsa = RSA()
    defineE(p,rsa.p)
    defineE(q,rsa.q)
    defineE(modu,rsa.public[1])
    defineE(pub,rsa.public[0])
    defineE(priv,rsa.private[0])
    buttons("normal")

'''
Connect the cipher function with the user interface
'''
def cifrar():
    global msg, cipher, pub, modu
    try:
        exp = int(pub.get())
        mod = int(modu.get())
        m = arreglarTexto(msg.get("1.0", "end-1c"))
    except:
        defineT(cipher, "Umm parece que ha habido un error")
        return
    
    if(exp == 0 or mod == 0 or m== ""):
        defineT(cipher, "Umm parece que ha habido un error")
        return
    
    defineT(cipher, "Cifrando...")
    buttons("disabled")
    rsa = RSA((exp,mod),(0,0))
    defineT(cipher, rsa.cipher(m))
    buttons("normal")
    
'''
Connect the icipher function with the user interface
'''    
def descifrar():
    global msg, cipher, priv, modu, m
    try:
        exp = int(priv.get())
        mod=int(modu.get())
        m = int(cipher.get("1.0", "end-1c"))
    except:
        defineT(msg, "Umm parece que ha habido un error")
        return
    
    if(exp == 0 or mod == 0 or m== ""):
        defineT(msg, "Umm parece que ha habido un error")
        return
    
    defineT(msg, "Descifrando...")
    buttons("disabled")
    rsa = RSA((0,mod),(exp,mod))
    defineT(msg, rsa.icipher(m))
    buttons("normal")

'''
It removes strange characters. Only allows characters in the dictionary

@param {String} Message to clean

@return {String} Message cleaned 
'''
def arreglarTexto(cadena):
    s = cadena.lower()

    s = s.replace("á","a")
    s = s.replace("é","e")
    s = s.replace("í","i")
    s = s.replace("ó","o")
    s = s.replace("ú","u")
    s = s.replace("ü","u")
    
    # bucle para eliminar caracteres que no sean letras ni espacios
    S = ''
    for i in s:
        k = ord(i)
        if (k>96 and k<123) or (k == 32):
            S = S + i
    return S

# =============================================================================
# CREATE INTERFACE
# =============================================================================
win = Tk()
win.resizable(False, False)
win.title("RSA")

prim = ttk.Frame(win)
prim.pack(pady=10, padx=10)
ttk.Label(prim,text='P: ').pack(side="left")
p = ttk.Entry(prim,width=14) 
p.pack(ipadx=1,side="left")
ttk.Label(prim,text='Q: ').pack(padx=(20,0),side="left") 
q = ttk.Entry(prim,width=14) 
q.pack(side="left")
bgen = ttk.Button(prim,text="Generar\nClaves",command=lambda: thread(generar))
bgen.pack(padx=(20,0),ipadx=1,side="left")

ttk.Label(win,text='Módulo:',justify="left").pack(padx=10, ipadx=190)
modu = ttk.Entry(win)
modu.pack(padx=10, pady=(0,20), ipadx=150)

ttk.Label(win,text='Pública:',justify="left").pack(padx=10, ipadx=190)
pub = ttk.Entry(win)
pub.pack(padx=10, pady=(0,20), ipadx=150)

ttk.Label(win,text='Privada:',justify="left").pack(padx=10, ipadx=190)
priv = ttk.Entry(win)
priv.pack(padx=10, pady=(0,20), ipadx=150)

ttk.Label(win,text='Mensaje:',justify="left").pack(padx=10, ipadx=190)
msg = Text(win, width = 60, height = 6)
msg.pack(padx=10,pady=(0,20))

ttk.Label(win,text='Cifrado:',justify="left").pack(padx=10, ipadx=190)
cipher = Text(win, width = 60, height = 6)
cipher.pack(padx=10,pady=(0,20))

acc = ttk.Frame(win)
acc.pack(pady=10, padx=10)
bCipher = ttk.Button(acc,text="Cifrar",command=lambda: thread(cifrar))
bCipher.pack(padx=(20,0),ipadx=1,side="left")
bDecipher = ttk.Button(acc,text="Descifrar",command=lambda: thread(descifrar))
bDecipher.pack(padx=(20,0),ipadx=1,side="left")

win.geometry("400x575+{}+{}".format(win.winfo_screenwidth()//2-400//2,
        win.winfo_screenheight()//2-575//2))
win.mainloop()